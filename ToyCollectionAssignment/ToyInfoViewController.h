//
//  ToyInfoViewController.h
//  ToyCollectionAssignment
//
//  Created by Justin on 2018-10-26.
//  Copyright © 2018 Justin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Toy.h"

NS_ASSUME_NONNULL_BEGIN

@interface ToyInfoViewController : UIViewController <UITextFieldDelegate>
@property Toy *toy;



-(void)setToyModel:(Toy*)toy;
@end

NS_ASSUME_NONNULL_END
