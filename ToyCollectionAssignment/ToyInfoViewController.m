//
//  ToyInfoViewController.m
//  ToyCollectionAssignment
//
//  Created by Justin on 2018-10-26.
//  Copyright © 2018 Justin. All rights reserved.
//

#import "ToyInfoViewController.h"

@interface ToyInfoViewController ()

@property (weak, nonatomic) IBOutlet UITextField *nameDetail;
@property (weak, nonatomic) IBOutlet UITextField *priceDetail;
@property (weak, nonatomic) IBOutlet UITextField *brandDetail;
@property (weak, nonatomic) IBOutlet UITextView *detailNotes;
@property (weak, nonatomic) IBOutlet UIImageView *imageDetail;

@end

@implementation ToyInfoViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _nameDetail.userInteractionEnabled = NO; // We do not want users editing the text field in this view
    _priceDetail.userInteractionEnabled = NO;
    _brandDetail.userInteractionEnabled = NO;
    _imageDetail.userInteractionEnabled = NO;
    
    if (self.toy) {
        _nameDetail.text = _toy.toyName;
        _brandDetail.text = _toy.toyBrand;
        _priceDetail.text = [[NSNumber numberWithFloat:_toy.toyPrice] stringValue];
        _detailNotes.text = _toy.toyNotes;
        _imageDetail.image = _toy.toyImage;
    }
}

- (void)setToyModel:(Toy *)toy {
    self.toy = toy;
}


@end
