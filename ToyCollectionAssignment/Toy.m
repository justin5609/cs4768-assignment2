//
//  Toy.m
//  ToyCollectionAssignment
//
//  Created by Justin on 2018-10-23.
//  Copyright © 2018 Justin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Toy.h"


@implementation Toy

-(id) initWithName:(NSString *)toyName andBrand:(NSString *)toyBrand andNotes:(NSString *)toyNotes andPrice:(int)toyPrice andImage:(UIImage *)toyImage
{
    self = [super init];
    if (self)
    {
        self.toyName = toyName;
        self.toyBrand = toyBrand;
        self.toyNotes = toyNotes;
        self.toyPrice = toyPrice;
        self.toyImage = toyImage;
        self.creationDate = [[NSDate alloc] init];
    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.toyName forKey:@"toyName"];
    [encoder encodeObject:self.toyBrand forKey:@"toyBrand"];
    [encoder encodeObject:[NSNumber numberWithFloat:self.toyPrice] forKey:@"toyPrice"];
    [encoder encodeObject:self.creationDate forKey:@"createdDate"];
    [encoder encodeObject:self.toyNotes forKey:@"toyNotes"];
    [encoder encodeObject:self.toyImage forKey:@"toyImage"];
}

-(id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    self.toyName  = [decoder decodeObjectForKey:@"toyName"];
    self.toyBrand = [decoder decodeObjectForKey:@"toyBrand"];
    self.toyPrice  = [[decoder decodeObjectForKey:@"toyPrice"] floatValue];
    self.creationDate = [decoder decodeObjectForKey:@"createdDate"];
    self.toyNotes = [decoder decodeObjectForKey:@"toyNotes"];
    self.toyImage = [decoder decodeObjectForKey:@"toyImage"];
    
    return self;
}


+(instancetype) toyWithInfo:(NSString *)toyName andBrand:(NSString *)toyBrand andNotes:(NSString *)toyNotes andPrice:(int)toyPrice andImage:(UIImage *)toyImage
{
    Toy *toy;
    
    return toy;
}
  
@end

