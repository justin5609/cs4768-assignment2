//
//  Toy.h
//  ToyCollectionAssignment
//
//  Created by Justin on 2018-10-23.
//  Copyright © 2018 Justin. All rights reserved.
//

@interface Toy : NSObject

@property (nonatomic, strong) NSString *toyName;
@property (nonatomic, strong) NSString *toyBrand;
@property (nonatomic, strong) NSString *toyNotes;
@property BOOL tapped;
@property NSDate *creationDate;

@property (nonatomic, assign) float toyPrice;

@property UIImage *toyImage;

-(id) initWithName:(NSString *)toyName andBrand:(NSString *)toyBrand andNotes:(NSString *)toyNotes andPrice:(int) toyPrice andImage:(UIImage *) toyImage;


+(instancetype) toyWithInfo:(NSString *)toyName andBrand:(NSString *)toyBrand andNotes:(NSString *)toyNotes andPrice:(int) toyPrice andImage:(UIImage *) toyImage;

@end

#ifndef Toy_h
#define Toy_h


#endif /* Toy_h */
