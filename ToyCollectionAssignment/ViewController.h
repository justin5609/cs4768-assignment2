//
//  ViewController.h
//  ToyCollectionAssignment
//
//  Created by Justin on 2018-10-19.
//  Copyright © 2018 Justin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Toy.h"


@interface ViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *brandField;
@property (weak, nonatomic) IBOutlet UITextField *priceField;
@property (weak, nonatomic) IBOutlet UITextField *additionalNotesField;
@property (weak, nonatomic) IBOutlet UIImageView *toyImage; // Display for image PICKER
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property UIImage *myImage;
@property Toy *toy;
- (IBAction)imageSelect:(UITapGestureRecognizer *)sender;


-(void) nameFieldDidEndEditing:(UITextField*)textField;

-(void) brandFieldDidEndEditing:(UITextField*)brandField;

-(void) priceFieldDidEndEditing:(UITextField*)priceField;

-(void) additNotesFieldDidEndEditing:(UITextField*)notesField;

-(BOOL) textFieldShouldReturn:(UITextField*)textField;

-(void) imagePickerControllerDidCancel:(UIImagePickerController *)picker;

-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;

@end
