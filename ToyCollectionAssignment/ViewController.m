//
//  ViewController.m
//  ToyCollectionAssignment
//
//  Created by Justin on 2018-10-19.
//  Copyright © 2018 Justin. All rights reserved.
//

#import "ViewController.h"
#import "Toy.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _nameField.delegate = self;
    _brandField.delegate = self;
    _priceField.delegate = self;
    _additionalNotesField.delegate = self;
    
   
    
    _toyImage.userInteractionEnabled = YES;
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void) nameFieldDidEndEditing:(UITextField*)textField { _nameField.text = textField.text; }

-(void) brandFieldDidEndEditing:(UITextField*)brandField { _brandField.text = brandField.text; }

-(void) priceFieldDidEndEditing:(UITextField*)priceField { _priceField.text = priceField.text; }

-(void) additNotesFieldDidEndEditing:(UITextField*)notesField { _additionalNotesField.text = notesField.text; }

-(BOOL) textFieldShouldReturn:(UITextField*)textField { [textField resignFirstResponder]; return YES; }

- (IBAction)imageSelect:(UITapGestureRecognizer*)sender {
   
    // Hide the keyboard
    
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init]; // Creates image picker
    
    
    
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary; // Source for picker is saved photos. Only allow photos to be picked, not taken
    
    // Make sure the picker is notified when the user picks an image
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:true completion:nil];
    
}

// UIImagePickerControllerDelegete

-(void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:true completion:nil]; // Dismiss the picker if the user canceled
    
}

-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    _myImage = [info objectForKey:UIImagePickerControllerOriginalImage]; // Get the selected image
    
    _toyImage.image = _myImage; // Set the view to the selected image
    
    [self dismissViewControllerAnimated:true completion:nil]; // Dismiss the picker
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if (sender != self.saveButton) return;
    self.toy = [[Toy alloc] init];
    _toy.toyName = _nameField.text;
    _toy.toyPrice = [_priceField.text floatValue];
    _toy.toyBrand = _brandField.text;
    _toy.toyNotes = _additionalNotesField.text;
    _toy.toyImage = _myImage;
}





@end





