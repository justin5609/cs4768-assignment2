//
//  ToyListTableViewController.h
//  ToyCollectionAssignment
//
//  Created by Justin on 2018-10-25.
//  Copyright © 2018 Justin. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface ToyListTableViewController : UITableViewController


- (IBAction)unwindToList:(UIStoryboardSegue *)segue;
@end

NS_ASSUME_NONNULL_END
