//
//  ToyListTableViewController.m
//  ToyCollectionAssignment
//
//  Created by Justin on 2018-10-25.
//  Copyright © 2018 Justin. All rights reserved.
//


/*
 
 Justin Delaney
 201222684
 
 CS4768
 Assignment 2
 
 Known bugs:
 - When selecting a toy in the table view, the detail view is blank and you must go back and press again for the toy info to load. This happens for each toy and toy table press
 
 */

#import "ToyListTableViewController.h"
#import "ToyInfoViewController.h"
#import "ViewController.h"
#import "Toy.h"

@interface ToyListTableViewController ()


@property NSMutableArray* toys;
@property Toy *tappedToy;
@property int totalValue;
@end

@implementation ToyListTableViewController



- (IBAction)unwindToList:(UIStoryboardSegue *)segue {
    ViewController *viewController = segue.sourceViewController;
    if (viewController.toy != nil) {
        [_toys addObject:viewController.toy];
        NSData *toyData = [NSKeyedArchiver archivedDataWithRootObject:_toys];
        [[NSUserDefaults standardUserDefaults] setObject:toyData forKey:@"toys"];
        
        [[self tableView] reloadData];
    }
}


-(void) loadData
{
    NSData *storedToys = [[NSUserDefaults standardUserDefaults] objectForKey:@"toys"];
    
    if (storedToys!= nil) {
        _toys = [NSKeyedUnarchiver unarchiveObjectWithData:storedToys];
    }
    
    else
    
    {

    Toy *toy1 = [[Toy alloc] init]; // Sample toy 1
    toy1.toyName = @"Star Wars Ship";
    toy1.toyPrice = 99.99;
    toy1.toyBrand = @"LEGO";
    toy1.toyNotes = @"Not assembled!";
    toy1.toyImage = [UIImage imageNamed:@"deathStar"];
    _totalValue += toy1.toyPrice; // Adding toy price to total value
    
    [self.toys addObject:toy1]; // Add toy to array
    
    Toy *toy2 = [[Toy alloc] init]; // Sample toy 2
    toy2.toyName = @"RC Car";
    toy2.toyPrice = 49.99;
    toy2.toyBrand = @"RCWorld";
    toy2.toyNotes = @"Needs new batteries";
    toy2.toyImage = [UIImage imageNamed:@"myRC"];
    _totalValue += toy2.toyPrice;
    
    [self.toys addObject:toy2];
    
    Toy *toy3 = [[Toy alloc] init];
    toy3.toyName = @"DS XL";
    toy3.toyPrice = 159.99;
    toy3.toyBrand = @"Nintendo";
    toy3.toyNotes = @"Good for road trips";
    toy3.toyImage = [UIImage imageNamed:@"myDS"];
    _totalValue += toy3.toyPrice;
    
    [self.toys addObject:toy3];
    
    Toy *toy4 = [[Toy alloc] init];
    toy4.toyName = @"PS4";
    toy4.toyBrand = @"Sony";
    toy4.toyPrice = 299.99;
    toy4.toyNotes = @"Need to buy new Call of Duty!";
    toy4.toyImage = [UIImage imageNamed:@"ps4"];
    _totalValue += toy4.toyPrice;
    
    [self.toys addObject:toy4];
        
    NSData *toyData = [NSKeyedArchiver archivedDataWithRootObject:_toys];
    [[NSUserDefaults standardUserDefaults] setObject:toyData forKey:@"toys"];
    }


}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.toys = [[NSMutableArray alloc] init];
    
    [self loadData];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self performSegueWithIdentifier:@"toyInfoView" sender:tableView];
    
    self.tappedToy = [self.toys objectAtIndex:indexPath.row];
    
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"toyInfoView"]) {
        ToyInfoViewController *viewController2 = [segue destinationViewController];
        [viewController2 setToy:self.tappedToy];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.toys count]; // Make a row for each toy in array
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ListProtoTypeCell" forIndexPath:indexPath];
    
    Toy *theToy = [self.toys objectAtIndex:indexPath.row];
    
    UIImageView *tableImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 100, 90)]; // Creates the image view to display thumbnail of toy image
    
    tableImage.image = theToy.toyImage;
    
    [cell addSubview:tableImage]; // add image to the cell
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, 100, 30)];  // Creates the labels to display some toy information
    UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 30, 100, 30)];
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 60, 90, 30)];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    
    NSDate *creationDate = [NSDate date]; // Creates a date that the toy is added
    
    NSString *strSelectedDate= [dateFormatter stringFromDate:creationDate]; // Formats the date to a string 
    
    
    nameLabel.text = theToy.toyName;
    priceLabel.text = [[NSNumber numberWithFloat:theToy.toyPrice] stringValue];
    dateLabel.text = strSelectedDate;
    
  
    [cell addSubview:priceLabel];
    [cell addSubview:nameLabel]; // add label to the cell
    [cell addSubview:dateLabel];
    
    return cell;
}


    
@end
